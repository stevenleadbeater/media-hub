# Media Hub
Media hub is the system component used to manage media playback on Ubuntu Touch devices. It is responsible for managing the media playback lifecycle, including the selection of the appropriate media player, and the routing of audio and video streams to the correct hardware.

## Architecture
The repository produces the following deb packages:
- [media-hub](debian%2Fmedia-hub.install)
- [libmedia-hub-qt](debian%2Flibmedia-hub-qt.install)
- [libmedia-hub-qt-dev](debian%2Flibmedia-hub-qt-dev.install)


### Clickable app integration
Apps can integrate with media-hub directly using the libmedia-hub-qt-dev package, or indirectly through the [QtMultiMedia](https://ubports.gitlab.io/docs/api-docs/?p=qtmultimedia%2Fqml-qtmultimedia-mediaplayer.html) package.

To integrate with media-hub using clickable, add the following to your c file:
```yaml
dependencies_target:
  - "libmedia-hub-qt-dev"
```
ensure the builder is set to `cmake` in clickable.yaml and update the `CMakeLists.txt` file adjacent to the code calling media-hub to include the following:
```cmake
target_link_libraries(${PLUGIN} MediaHub)
```
if you are adding functions to the libmedia-hub-qt-dev library, you will need to load the changed header files into the clickable build environment. This can be done by  forking the media-hub repository in gitlab to your own account and adding the following to the clickable.yaml file:
```yaml
image_setup:
  run:
    - wget https://gitlab.com/<YOUR_GITGLAB_ACCOUNT>/media-hub/-/raw/main/src/lib/MediaHub/player.h -O /usr/include/MediaHub/player.h
```
you will need to add a wget call for each of the header files you have changed in `src/lib/MediaHub`.

#### code sample
The ubports_app_template for [ContentHub](https://github.com/fulvio999/ubports_app_templates/tree/master/ContentHub) has an example of how to include a plugin with a QML app. To call a libmedia-hub-qt-dev function from a QML file, you will need to create a plugin in C++ that can be called from QML as per that example. This header file is used to call the `setSleepAfter` funtion:
```cpp
#include <MediaHub/Player>
#include <memory>


class SleepUtils: public QObject {
    Q_OBJECT

    std::shared_ptr<lomiri::MediaHub::Player> m_hubPlayerSession;
    QString m_sessionUuid;

public:
    SleepUtils();
   ~SleepUtils() = default;
   /* IMPORTANT: Without Q_INVOKABLE as prefix, the method is NOT exposed to QML (ie: is non invokable) causing error like:
      TypeError: Property <metho-name> of object <pluginName>(0x284d8d0) is not a function
   */

   Q_INVOKABLE void sleepAfter(double seconds);
};

```
with the corresponding cpp file:
```cpp
#include <QDebug>
#include <QtQml>
#include <QtQml/QQmlContext>
#include "fileutils.h"
#include "plugin.h"
#include <MediaHub/Player>

SleepUtils::SleepUtils() {

}

void SleepUtils::sleepAfter(double seconds) {
    qDebug() << Q_FUNC_INFO;
// Only one player session needed
    if (m_hubPlayerSession == nullptr) {

        try {
            qDebug() << "Starting a new media-hub player session...";
            m_hubPlayerSession = std::make_shared<lomiri::MediaHub::Player>(true);
            qDebug() << "got media hub session!";
        }
        catch (const std::runtime_error &e) {
            qWarning() << "Failed to start a new media-hub player session: " << e.what();
            return;
        }


        // Get the player session UUID, so we can suspend/restore our session when the ApplicationState
        // changes
        m_sessionUuid = m_hubPlayerSession->uuid();
        qDebug() << "media hub UUID " << m_sessionUuid;
    }
    m_hubPlayerSession->setSleepAfter(seconds * 1000);
    qDebug() << "media hub call " << m_hubPlayerSession->sleepAfter();
}

```
There are 2 overloads of the `std::make_shared<lomiri::MediaHub::Player>()` constructor, one with no arguments and one with a `bool` argument. The `bool` argument is used to enable or disable sharing the player session across the app. If the `bool` argument is `true`, the player session will be shared across the app, this means calling the `setSleepAfter` function in one part of the app will affect the player session in another part of the app. If the `bool` argument is `false`, the player session will not be shared across the app, this means calling the `setSleepAfter` function from the plugin will pause playback started in a QtMultiMedia player defined in QML. The default behaviour with the no arg constructor is to use separate sessions


## Coding Convention

NOTE: the media-hub code did not start out with the following coding convention, but it is being introduced to try and
converge on a standard from this point forward:

https://google-styleguide.googlecode.com/svn/trunk/cppguide.html

Deviations from the Google Style Guide above:

1. We will agree to maximum of 100 characters in a single line instead of 80.


## Build
### Local
```shell
mkdir build
cd build
cmake ..
make
```
#### To Run From Console
    
```shell
CORE_MEDIA_SERVICE_VIDEO_SINK_NAME=mirsink CORE_MEDIA_SERVICE_AUDIO_SINK_NAME=pulsesink src/core/media/media-hub-server
```
### Ubuntu Touch Devices
See the [crossbuilder](https://github.com/ubports/crossbuilder) project for more information on how to build for a device.
```shell
crossbuilder --password=YOUR_SUDO_PASSWORD
```

## Testing on devices
after running crossbuilder, unpack the tar file in the root directory to get the deb files out and install `media-hub<version>.deb` on the device. You can copy it to the device with `scp` and install it with `sudo dpkg -i media-hub<version>.deb`. You will need to reboot the device after this for the changes to take effect.

## To Run Unit Tests
```shell
cd build/tests/acceptance-tests
./service_acceptance_test

cd build/tests/unit-tests
./test-gstreamer-engine
```
